# Liquid主题引擎

## 介绍
使用 C# 开发的基于 Liquid 语言的多主题引擎。

## 渲染一个主题视图
```csharp
//主题文件存放地址
var themeRootPath = Path.GetFullPath(Path.Combine("wwwroot", "themes"));
var fileProvider = new PhysicalFileProvider(themeRootPath);
//主题引擎配置
var themeEnginOptions = new LiquidThemeEngineOptions();
themeEnginOptions.FileProvider = new LiquidThemeFileProvider(fileProvider);//配置主题文件所在的位置
//主题名称（主题目录名称）
var theme = "default";
//视图名称，对应 templates 目录下的文件
var view = "index";
//视图模型，可以在模板中使用
var model = new
{
    Id = 123,
    Age = 10,
    CreatedTime = DateTime.Now
};
//创建一个模板上下文
var context = new LiquidThemeTemplateContext(themeEnginOptions, theme, model);
//设置多语言环境
context.CultureInfo = new CultureInfo("zh-hans");
//设置模板变量
context.SetValue("title", "标题的取值");
//渲染结果
var html = await LiquidThemeViewRender.RenderAsync(theme, view, context);
Console.WriteLine("渲染结果：{0}", html);
```

## Liquid 语言文档

[Liquid 模板语言中文文档](https://www.coderbusy.com/liquid/)

[Liquid template language](https://shopify.github.io/liquid/)

## 主题结构

一个主题包含以下几个文件夹：

+ assets
+ config
+ layout
+ locales
+ sections
+ snippets
+ templates

### assets 文件夹

资源目录。主题所需的 css、js、图片等文件存放在这个文件中，通过 ```asset_url``` 标签获取访问路径。

如果 ```assets``` 目录下有一个名为 ```logo.png``` 的文件，在模板中可以通过以下代码显示该图片：

```liquid
<img src="{{ 'logo.png' | asset_url }}">
```

### config 文件夹

主题配置文件目录。包含以下三个文件：

+ settings_schema.json
+ settings_presets.json
+ settings_data.json

#### settings_schema.json

该文件保存主题元信息和设置定义信息，内容为 JSON 数组字符串。

主题元信息包括：主题名称、作者、版本等。当 name=theme_info 时代表该条数据是主题辕信息。

#### settings_presets.json

主题预设设置，保存由开发者预定义的主题设置。可以有多套，设置一个主题的多个不同风格。用户可以快速选择一个风格。

#### settings_data.json

当前使用的主题设置。

### layout 文件夹

保存主题的布局文件，当模板未通过 ```layout``` 标签指定布局文件时，会默认使用 ```theme``` 。布局文件中可以使用 ```render_body``` 标签输出模板内容。

### locales 文件夹

多语言文件保存于此。

### sections 文件夹

模板节保存于此。需要在 ```settings_data.json``` 中引用该文件夹中的文件，并通过 ```section``` 标签渲染一个模板节。

### snippets 文件夹

代码段保存于此。可以在模板中使用 ```render``` 标签调用该目录下的文件。

### templates 文件夹

模板文件保存于此。