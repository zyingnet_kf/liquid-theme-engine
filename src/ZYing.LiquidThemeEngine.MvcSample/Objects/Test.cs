﻿using ZYing.LiquidThemeEngine.Objects;

namespace ZYing.LiquidThemeEngine.MvcSample.Objects;

public class Test : ILiquidObject
{
    public int Year { get; set; }
    public string Name { get; set; }
}