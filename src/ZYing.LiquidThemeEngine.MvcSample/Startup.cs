using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using ZYing.LiquidThemeEngine.AspNetCore;

namespace ZYing.LiquidThemeEngine.MvcSample;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddControllersWithViews();
        services.AddMemoryCache(options => { });
        services.AddOptions();
        services.AddTransient<IConfigureOptions<LiquidThemeEngineOptions>, ConfigureLiquidThemeEngineOptions>();
        services.AddSingleton<ILiquidThemeManager>(s =>
        {
            var options = s.GetRequiredService<IOptions<LiquidThemeEngineOptions>>();
            return new LiquidThemeManager(options.Value);
        });

        services.AddLiquidThemeEngine();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
            app.UseDeveloperExceptionPage();
        else
            app.UseExceptionHandler("/Home/Error");
        app.UseStaticFiles();

        app.UseRouting();

        app.UseAuthorization();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllerRoute(
                "default",
                "{controller=Home}/{action=Index}/{id?}");
        });
    }
}