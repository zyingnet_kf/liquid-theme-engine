﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using ZYing.LiquidThemeEngine.FileProviders;

namespace ZYing.LiquidThemeEngine.MvcSample;

public class ConfigureLiquidThemeEngineOptions : ConfigureOptions<LiquidThemeEngineOptions>
{
    public ConfigureLiquidThemeEngineOptions(IWebHostEnvironment env) :
        base(options => { options.FileProvider = new LiquidThemeFileProvider(env.WebRootFileProvider, "themes"); })
    {
    }
}