﻿using Newtonsoft.Json;

namespace ZYing.LiquidThemeEngine;

/// <summary>
///     主题信息
/// </summary>
public class LiquidThemeInfo
{
    /// <summary>
    ///     主题名称
    /// </summary>
    [JsonProperty("theme_name")]
    public string ThemeName { get; set; }

    /// <summary>
    ///     主题的作者
    /// </summary>
    [JsonProperty("theme_author")]
    public string ThemeAuthor { get; set; }

    /// <summary>
    ///     主题的版本号
    /// </summary>
    [JsonProperty("theme_version")]
    public string ThemeVersion { get; set; }

    /// <summary>
    ///     商家可以在其中找到主题文档的 URL
    /// </summary>
    [JsonProperty("theme_documentation_url")]
    public string ThemeDocumentationUrl { get; set; }

    /// <summary>
    ///     商家可以联系以获取主题支持的电子邮件地址
    /// </summary>
    [JsonProperty("theme_support_email")]
    public string ThemeSupportEmail { get; set; }

    /// <summary>
    ///     商家可以在其中找到对该主题的支持的 URL
    /// </summary>
    [JsonProperty("theme_support_url")]
    public string ThemeSupportUrl { get; set; }
}