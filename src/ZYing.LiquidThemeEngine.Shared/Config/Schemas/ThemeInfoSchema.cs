﻿using Newtonsoft.Json;

namespace ZYing.LiquidThemeEngine.Config.Schemas;

public class ThemeInfoSchema : ThemeSchema
{
    [JsonProperty("theme_name")] public string ThemeName { get; set; }

    [JsonProperty("theme_author")] public string ThemeAuthor { get; set; }

    [JsonProperty("theme_version")] public string ThemeVersion { get; set; }

    [JsonProperty("theme_documentation_url")]
    public string ThemeDocumentationUrl { get; set; }

    [JsonProperty("theme_support_email")] public string ThemeSupportEmail { get; set; }

    [JsonProperty("theme_support_url")] public string ThemeSupportUrl { get; set; }
}