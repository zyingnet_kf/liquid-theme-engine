﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ZYing.LiquidThemeEngine.Config.Schemas;

public class ThemeSettingSchema : ThemeSchema
{
    [JsonProperty("settings")] public List<ThemeSettingItem> Settings { get; set; }
}