﻿using System.Collections.Generic;
using Newtonsoft.Json;
using ZYing.LiquidThemeEngine.Locales;

namespace ZYing.LiquidThemeEngine.Config.Schemas;

/// <summary>
///     主题模式集合
/// </summary>
public class ThemeSchemaCollection : List<ThemeSchema>
{
    public static ThemeSchemaCollection Parse(string value)
    {
        return JsonConvert.DeserializeObject<ThemeSchemaCollection>(value);
    }

    public LiquidThemeInfo GetThemeInfo()
    {
        foreach (var item in this)
            if (item.Name is FixedLocalizableText { Text: "theme_info" } &&
                item is ThemeInfoSchema info)
                return new LiquidThemeInfo
                {
                    ThemeName = info.ThemeName,
                    ThemeAuthor = info.ThemeAuthor,
                    ThemeVersion = info.ThemeVersion,
                    ThemeDocumentationUrl = info.ThemeDocumentationUrl,
                    ThemeSupportEmail = info.ThemeSupportEmail,
                    ThemeSupportUrl = info.ThemeSupportUrl
                };

        return null;
    }

    public string ToJson()
    {
        return JsonConvert.SerializeObject(this);
    }
}