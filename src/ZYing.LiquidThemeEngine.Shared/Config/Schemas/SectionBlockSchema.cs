﻿using System.Collections.Generic;
using Newtonsoft.Json;
using ZYing.LiquidThemeEngine.Locales;

namespace ZYing.LiquidThemeEngine.Config.Schemas;

public class SectionBlockSchema
{
    /// <summary>
    ///     The block type. This is a free-form string that you can use as an identifier. You can access this value through the
    ///     type attribute of the Liquid block object.
    /// </summary>
    [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
    public string Type { get; set; }

    /// <summary>
    ///     The block name, which will show as the block title in the theme editor.
    /// </summary>
    [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
    public ILocalizableText Name { get; set; }

    /// <summary>
    ///     The number of blocks of this type that can be used.
    /// </summary>
    [JsonProperty("limit", NullValueHandling = NullValueHandling.Ignore)]
    public int? Limit { get; set; }

    [JsonProperty("settings", NullValueHandling = NullValueHandling.Ignore)]
    public List<ThemeSettingItem> Settings { get; set; }
}