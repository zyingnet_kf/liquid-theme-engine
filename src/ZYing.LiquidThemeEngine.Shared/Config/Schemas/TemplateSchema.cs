﻿using Newtonsoft.Json;

namespace ZYing.LiquidThemeEngine.Config.Schemas;

public class TemplateSchema
{
    [JsonProperty("page_size", NullValueHandling = NullValueHandling.Ignore)]
    public int? PageSize { get; set; }

    public static TemplateSchema Parse(string value)
    {
        return JsonConvert.DeserializeObject<TemplateSchema>(value);
    }
}