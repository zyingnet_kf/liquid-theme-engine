﻿using System.Collections.Generic;
using Newtonsoft.Json;
using ZYing.LiquidThemeEngine.Locales;

namespace ZYing.LiquidThemeEngine.Config.Schemas;

[JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
public class SectionSchema
{
    /// <summary>
    ///     展示用的标题
    /// </summary>
    [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
    public ILocalizableText Name { get; set; }

    /// <summary>
    ///     渲染输出后的外包装标签，默认为 div。
    /// </summary>
    [JsonProperty("tag", NullValueHandling = NullValueHandling.Ignore)]
    public string Tag { get; set; }

    /// <summary>
    ///     渲染输出后的外包装类，默认为空
    /// </summary>
    [JsonProperty("class", NullValueHandling = NullValueHandling.Ignore)]
    public string Class { get; set; }

    /// <summary>
    ///     默认情况下，不限制一个 section 在 template 中呈现的次数。你可以指定一个限制。
    /// </summary>
    [JsonProperty("limit", NullValueHandling = NullValueHandling.Ignore)]
    public int? Limit { get; set; }

    /// <summary>
    ///     适用于这个 section 的配置设置。
    ///     当设置项类型为 text，id 为 title 时，在编辑器展示设置项的 lable。
    /// </summary>
    [JsonProperty("settings", NullValueHandling = NullValueHandling.Ignore)]
    public List<ThemeSettingItem> Settings { get; set; }

    /// <summary>
    ///     可以为 section 创建块(block)。块是可重用的内容模块，可以在节中添加、删除和重新排序。
    ///     https://shopify.dev/themes/architecture/sections/section-schema#blocks
    /// </summary>
    [JsonProperty("blocks", NullValueHandling = NullValueHandling.Ignore)]
    public List<SectionBlockSchema> Blocks { get; set; }

    /// <summary>
    ///     默认情况下，每个 section 限制 16 个 block。可以用 max_blocks 指定该设置。
    /// </summary>
    [JsonProperty("max_blocks", NullValueHandling = NullValueHandling.Ignore)]
    public int? MaxBlocks { get; set; }

    /// <summary>
    ///     Section presets are default configurations of sections
    ///     that allow merchants to easily add a section to a JSON template through the theme editor.
    ///     They aren't related to theme styles that are defined in settings_data.json.
    /// </summary>
    [JsonProperty("presets", NullValueHandling = NullValueHandling.Ignore)]
    public List<SectionPresetModel> Presets { get; set; }

    /// <summary>
    ///     If you statically render a section, then you can define a default configuration with the default object, which has
    ///     the same attributes as the preset object.
    /// </summary>
    [JsonProperty("default", NullValueHandling = NullValueHandling.Ignore)]
    public SectionDefaultModel Default { get; set; }

    /// <summary>
    ///     You can restrict a section to certain templates by specifying those templates through the templates attribute.
    /// </summary>
    [JsonProperty("templates", NullValueHandling = NullValueHandling.Ignore)]
    public List<string> Templates { get; set; }

    public static SectionSchema Parse(string value)
    {
        return JsonConvert.DeserializeObject<SectionSchema>(value);
    }
}