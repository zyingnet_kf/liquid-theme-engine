﻿using Newtonsoft.Json;
using ZYing.LiquidThemeEngine.Locales;

namespace ZYing.LiquidThemeEngine.Config.Schemas.Settings;

public class TextareaSettingItem : StandardSettingItem
{
    public override string Type => "textarea";
    [JsonProperty("placeholder")] public ILocalizableText Placeholder { get; set; }
    [JsonProperty("default")] public ILocalizableText Default { get; set; }
}