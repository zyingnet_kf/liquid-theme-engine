﻿using Newtonsoft.Json;
using ZYing.LiquidThemeEngine.Locales;

namespace ZYing.LiquidThemeEngine.Config.Schemas.Settings;

public class OptionItem
{
    [JsonProperty("label")] public ILocalizableText Label { get; set; }

    [JsonProperty("value")] public string Value { get; set; }

    [JsonProperty("group")] public string Group { get; set; }
}