﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ZYing.LiquidThemeEngine.Config.Schemas.Settings;

public class RadioSettingItem : StandardSettingItem
{
    public override string Type => "radio";
    [JsonProperty("options")] public List<OptionItem> Options { get; set; }
    [JsonProperty("default")] public string Default { get; set; }
}