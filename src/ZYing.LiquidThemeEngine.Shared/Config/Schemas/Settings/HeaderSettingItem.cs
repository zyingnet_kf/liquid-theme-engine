﻿using Newtonsoft.Json;
using ZYing.LiquidThemeEngine.Locales;

namespace ZYing.LiquidThemeEngine.Config.Schemas.Settings;

public class HeaderSettingItem : ThemeSettingItem
{
    public override string Type => "header";

    [JsonProperty("content")] public ILocalizableText Content { get; set; }
}