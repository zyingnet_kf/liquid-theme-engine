﻿using Newtonsoft.Json;
using ZYing.LiquidThemeEngine.Locales;

namespace ZYing.LiquidThemeEngine.Config.Schemas.Settings;

public class RichTextSettingItem : StandardSettingItem
{
    public override string Type => "richtext";
    [JsonProperty("default")] public ILocalizableText Default { get; set; }
}