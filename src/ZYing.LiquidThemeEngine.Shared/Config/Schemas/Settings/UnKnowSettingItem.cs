﻿using Newtonsoft.Json.Linq;

namespace ZYing.LiquidThemeEngine.Config.Schemas.Settings;

internal class UnKnowSettingItem : ThemeSettingItem
{
    public UnKnowSettingItem(JObject obj, string type)
    {
        Type = type;
    }

    public override string Type { get; }
}