﻿using Newtonsoft.Json;

namespace ZYing.LiquidThemeEngine.Config.Schemas.Settings;

public class RangSettingItem : StandardSettingItem
{
    public override string Type => "rang";

    [JsonProperty("min")] public int Min { get; set; }

    [JsonProperty("max")] public int Max { get; set; }

    [JsonProperty("step")] public int Step { get; set; }

    [JsonProperty("unit")] public string Unit { get; set; }

    [JsonProperty("default")] public int Default { get; set; }
}