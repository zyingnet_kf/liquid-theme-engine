﻿using Newtonsoft.Json;
using ZYing.LiquidThemeEngine.Locales;

namespace ZYing.LiquidThemeEngine.Config.Schemas.Settings;

public class ParagraphSettingItem : ThemeSettingItem
{
    public override string Type => "paragraph";

    [JsonProperty("content")] public ILocalizableText Content { get; set; }
}