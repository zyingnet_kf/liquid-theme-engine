﻿using Newtonsoft.Json;

namespace ZYing.LiquidThemeEngine.Config.Schemas.Settings;

/// <summary>
///     单选框
/// </summary>
public class CheckboxSettingItem : StandardSettingItem
{
    public override string Type => "checkbox";

    [JsonProperty("default")] public bool Default { get; set; }
}