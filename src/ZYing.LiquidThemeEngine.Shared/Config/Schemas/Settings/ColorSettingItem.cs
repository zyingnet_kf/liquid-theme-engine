﻿using Newtonsoft.Json;

namespace ZYing.LiquidThemeEngine.Config.Schemas.Settings;

public class ColorSettingItem : StandardSettingItem
{
    public override string Type => "color";
    [JsonProperty("default")] public string Default { get; set; }
}