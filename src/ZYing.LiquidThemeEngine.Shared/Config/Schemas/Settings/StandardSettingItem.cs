﻿using Newtonsoft.Json;
using ZYing.LiquidThemeEngine.Locales;

namespace ZYing.LiquidThemeEngine.Config.Schemas.Settings;

/// <summary>
///     标准设置项
/// </summary>
public abstract class StandardSettingItem : ThemeSettingItem
{
    [JsonProperty("id")] public string Id { get; set; }

    [JsonProperty("label")] public ILocalizableText Label { get; set; }

    [JsonProperty("info")] public ILocalizableText Info { get; set; }
}