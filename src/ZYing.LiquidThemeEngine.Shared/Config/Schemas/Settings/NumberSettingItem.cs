﻿using Newtonsoft.Json;
using ZYing.LiquidThemeEngine.Locales;

namespace ZYing.LiquidThemeEngine.Config.Schemas.Settings;

public class NumberSettingItem : StandardSettingItem
{
    public override string Type => "number";
    [JsonProperty("placeholder")] public ILocalizableText Placeholder { get; set; }
    [JsonProperty("default")] public int Default { get; set; }
}