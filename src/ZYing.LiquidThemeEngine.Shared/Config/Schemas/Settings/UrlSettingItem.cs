﻿using Newtonsoft.Json;

namespace ZYing.LiquidThemeEngine.Config.Schemas.Settings;

public class UrlSettingItem : StandardSettingItem
{
    public override string Type => "url";
    [JsonProperty("default")] public string Default { get; set; }
}