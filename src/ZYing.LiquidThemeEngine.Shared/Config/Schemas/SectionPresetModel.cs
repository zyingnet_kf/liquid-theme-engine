﻿using System.Collections.Generic;
using Newtonsoft.Json;
using ZYing.LiquidThemeEngine.Config.Values;
using ZYing.LiquidThemeEngine.Locales;

namespace ZYing.LiquidThemeEngine.Config.Schemas;

public class SectionPresetModel
{
    /// <summary>
    ///     The preset name, which will show in the Add section portion of the theme editor.
    /// </summary>
    [JsonProperty("name")]
    public ILocalizableText Name { get; set; }

    /// <summary>
    ///     A list of default values for any settings you might want to populate.
    ///     Each entry should include the setting name and the value.
    /// </summary>
    [JsonProperty("settings")]
    public SortedDictionary<string, object> Settings { get; set; }

    /// <summary>
    ///     A list of default blocks that you might want to include.
    ///     Each entry should be an object with attributes of type and settings.
    ///     The type attribute value should reflect the type of the block that you want to include,
    ///     and the settings object should be in the same format as the settings attribute above.
    /// </summary>
    [JsonProperty("blocks")]
    public List<SectionBlockSetting> Blocks { get; set; }
}