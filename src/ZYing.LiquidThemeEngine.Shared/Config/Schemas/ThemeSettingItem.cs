﻿using Newtonsoft.Json;
using ZYing.LiquidThemeEngine.Json;

namespace ZYing.LiquidThemeEngine.Config.Schemas;

[JsonConverter(typeof(ThemeSettingItemJsonConverter))]
[JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
public abstract class ThemeSettingItem
{
    [JsonProperty("type")] public abstract string Type { get; }
}