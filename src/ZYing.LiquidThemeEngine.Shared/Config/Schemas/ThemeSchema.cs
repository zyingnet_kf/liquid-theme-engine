﻿using Newtonsoft.Json;
using ZYing.LiquidThemeEngine.Json;
using ZYing.LiquidThemeEngine.Locales;

namespace ZYing.LiquidThemeEngine.Config.Schemas;

/// <summary>
///     主题模式
/// </summary>
[JsonConverter(typeof(ThemeSchemaJsonConverter))]
public abstract class ThemeSchema
{
    /// <summary>
    ///     模式名称
    /// </summary>
    [JsonProperty("name")]
    public ILocalizableText Name { get; set; }
}