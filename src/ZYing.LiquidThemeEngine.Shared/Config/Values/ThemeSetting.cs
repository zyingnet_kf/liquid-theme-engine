﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ZYing.LiquidThemeEngine.Config.Values;

/// <summary>
///     主题设置
/// </summary>
public class ThemeSetting
{
    /// <summary>
    ///     主题全局设置
    /// </summary>
    [JsonProperty("settings")]
    public SortedDictionary<string, object> Settings { get; set; }

    /// <summary>
    ///     主题章节设置
    /// </summary>
    [JsonProperty("sections")]
    public SortedDictionary<string, SectionSetting> Sections { get; set; }

    /// <summary>
    ///     首页内容列表(为 Section 的 id 列表。)
    /// </summary>
    [JsonProperty("content_for_index")]
    public List<string> ContentForIndex { get; set; }

    public static ThemeSetting Parse(string value)
    {
        return JsonConvert.DeserializeObject<ThemeSetting>(value);
    }

    public string ToJson()
    {
        return JsonConvert.SerializeObject(this, Formatting.Indented);
    }
}