﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ZYing.LiquidThemeEngine.Config.Values;

/// <summary>
///     章节设置
/// </summary>
[JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
public class SectionSetting
{
    /// <summary>
    ///     章节类型，对应 sections 目录下的模板
    /// </summary>
    [JsonProperty("type")]
    public string Type { get; set; }

    /// <summary>
    ///     章节设置值
    /// </summary>
    [JsonProperty("settings")]
    public SortedDictionary<string, object> Settings { get; set; }

    /// <summary>
    ///     章节包含的显示块
    /// </summary>
    [JsonProperty("blocks")]
    public SortedDictionary<string, SectionBlockSetting> Blocks { get; set; }

    /// <summary>
    ///     章节包含显示块的显示顺序
    /// </summary>
    [JsonProperty("block_order")]
    public List<string> BlockOrder { get; set; }
}