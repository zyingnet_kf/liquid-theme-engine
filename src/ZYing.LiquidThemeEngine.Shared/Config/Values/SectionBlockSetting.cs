﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ZYing.LiquidThemeEngine.Config.Values;

/// <summary>
///     章节块设置
/// </summary>
public class SectionBlockSetting
{
    /// <summary>
    ///     章节块类型
    /// </summary>
    [JsonProperty("type")]
    public string Type { get; set; }

    /// <summary>
    ///     章节块设置值
    /// </summary>
    [JsonProperty("settings")]
    public SortedDictionary<string, object> Settings { get; set; }
}