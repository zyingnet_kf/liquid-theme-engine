﻿using System.Collections.Generic;

namespace ZYing.LiquidThemeEngine.Locales;

public interface ILocaleDictionary : IReadOnlyDictionary<string, string>
{
}