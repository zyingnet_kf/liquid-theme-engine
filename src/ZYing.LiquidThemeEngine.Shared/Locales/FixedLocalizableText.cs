﻿using System;
using System.Diagnostics;

namespace ZYing.LiquidThemeEngine.Locales;

[DebuggerDisplay("{DebuggerDisplay,nq}")]
public class FixedLocalizableText : ILocalizableText
{
    public FixedLocalizableText(string text)
    {
        Text = text ?? throw new ArgumentNullException(nameof(text));
    }

    public string Text { get; }

    [DebuggerBrowsable(DebuggerBrowsableState.Never)]
    private string DebuggerDisplay => $"FixedLocaleText : \"{ToString()}\"";

    public string Locale(string lang)
    {
        return Text;
    }

    public override string ToString()
    {
        return Text;
    }
}