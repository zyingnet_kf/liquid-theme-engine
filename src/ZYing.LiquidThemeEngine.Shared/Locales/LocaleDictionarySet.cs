﻿using System;
using System.Collections.Concurrent;

namespace ZYing.LiquidThemeEngine.Locales;

public class LocaleDictionarySet : ConcurrentDictionary<string, ILocaleDictionary>
{
    public LocaleDictionarySet() : base(StringComparer.OrdinalIgnoreCase)
    {
    }
}