﻿using Newtonsoft.Json;
using ZYing.LiquidThemeEngine.Json;

namespace ZYing.LiquidThemeEngine.Locales;

/// <summary>
///     支持本地化的文本
/// </summary>
[JsonConverter(typeof(LocalizableTextJsonConverter))]
public interface ILocalizableText
{
    string Locale(string lang);
}