﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace ZYing.LiquidThemeEngine.Locales;

public class DictionaryLocalizableText : SortedDictionary<string, string>, ILocalizableText
{
    public DictionaryLocalizableText() : base(StringComparer.OrdinalIgnoreCase)
    {
    }

    public string Locale(string lang)
    {
        if (Count == 0) return string.Empty;

        if (TryGetValue(lang, out var ret)) return ret;

        return Values.First();
    }

    public override string ToString()
    {
        var lang = CultureInfo.CurrentCulture.Name;
        return Locale(lang);
    }
}