﻿using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace ZYing.LiquidThemeEngine.Locales;

public class LocaleDictionary : SortedDictionary<string, string>, ILocaleDictionary
{
    public static LocaleDictionary Empty { get; } = new();

    internal void MergeJsonIntoDictionary(JToken token)
    {
        if (token is JObject obj)
            foreach (var item in obj.Properties())
                MergeJsonIntoDictionary(item.Value);
        else if (token is JArray array)
            foreach (var item in array.Children())
                MergeJsonIntoDictionary(item);
        else if (token.Type == JTokenType.String) this[token.Path] = token.Value<string>();
    }
}