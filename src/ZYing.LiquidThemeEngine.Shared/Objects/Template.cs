﻿namespace ZYing.LiquidThemeEngine.Objects;

public sealed class Template : ILiquidObject
{
    public string FullPath { get; set; }
    public string Directory { get; set; }
    public string Name { get; set; }
    public string Suffix { get; set; }
}