﻿using System.Collections.Generic;

namespace ZYing.LiquidThemeEngine.Objects;

public class Block : ILiquidObject
{
    public string Id { get; set; }
    public IDictionary<string, object> Settings { get; set; }
    public string Type { get; set; }
}