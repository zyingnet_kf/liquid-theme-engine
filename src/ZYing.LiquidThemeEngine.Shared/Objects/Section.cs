﻿using System.Collections.Generic;

namespace ZYing.LiquidThemeEngine.Objects;

public class Section : ILiquidObject
{
    public string Id { get; set; }
    public IReadOnlyDictionary<string, object> Settings { get; set; }
    public List<Block> Blocks { get; set; }
}