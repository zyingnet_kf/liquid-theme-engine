﻿using System;
using System.Collections.Concurrent;
using Newtonsoft.Json.Linq;
using ZYing.LiquidThemeEngine.Config.Schemas;
using ZYing.LiquidThemeEngine.Config.Schemas.Settings;

namespace ZYing.LiquidThemeEngine.Json;

public class ThemeSettingItemJsonConverter : JsonCreationConverter<ThemeSettingItem>
{
    private static readonly ConcurrentDictionary<string, Func<ThemeSettingItem>> _factorySet = new();

    public static void Register(string type, Func<ThemeSettingItem> factory)
    {
        if (string.IsNullOrWhiteSpace(type)) throw new ArgumentNullException(nameof(type));
        if (factory == null) throw new ArgumentNullException(nameof(factory));
        _factorySet[type] = factory;
    }

    protected override ThemeSettingItem Create(Type objectType, JObject jObject)
    {
        var type = jObject.Value<string>("type");
        switch (type)
        {
            case "checkbox":
                return new CheckboxSettingItem();
            case "number":
                return new NumberSettingItem();
            case "radio":
                return new RadioSettingItem();
            case "rang":
                return new RangSettingItem();
            case "select":
                return new SelectSettingItem();
            case "text":
                return new TextSettingItem();
            case "textarea":
                return new TextareaSettingItem();
            case "color":
                return new ColorSettingItem();
            case "html":
                return new HtmlSettingItem();
            case "richtext":
                return new RichTextSettingItem();
            case "url":
                return new UrlSettingItem();
            case "header":
                return new HeaderSettingItem();
            case "paragraph":
                return new ParagraphSettingItem();
        }

        if (_factorySet.TryGetValue(type, out var factory)) return factory();

        return new UnKnowSettingItem(jObject, type);
    }
}