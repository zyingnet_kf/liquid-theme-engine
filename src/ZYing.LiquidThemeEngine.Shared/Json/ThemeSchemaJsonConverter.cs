﻿using System;
using Newtonsoft.Json.Linq;
using ZYing.LiquidThemeEngine.Config.Schemas;

namespace ZYing.LiquidThemeEngine.Json;

public class ThemeSchemaJsonConverter : JsonCreationConverter<ThemeSchema>
{
    protected override ThemeSchema Create(Type objectType, JObject jObject)
    {
        if (jObject.TryGetValue("name", out var value) && value.Type == JTokenType.String &&
            value.Value<string>() == "theme_info") return new ThemeInfoSchema();
        return new ThemeSettingSchema();
    }
}