﻿namespace ZYing.LiquidThemeEngine;

/// <summary>
///     Liquid主题引擎常量定义
/// </summary>
public static class Constants
{
    /// <summary>
    ///     模板文件的扩展名
    /// </summary>
    public const string TemplateExtension = ".liquid";

    /// <summary>
    ///     默认布局文件
    /// </summary>
    public const string DefaultLayout = "theme";

    /// <summary>
    ///     默认分页大小
    /// </summary>
    public const int DefaultPageSize = 10;

    /// <summary>
    ///     最大分页大小
    /// </summary>
    public const int MaxPageSize = 50;

    /// <summary>
    ///     最小分页大小
    /// </summary>
    public const int MinPageSize = 1;

    /// <summary>
    ///     内置对象名称
    /// </summary>
    public static class ObjectNames
    {
        /// <summary>
        ///     模板对象访问名称
        /// </summary>
        public const string Template = "template";

        /// <summary>
        ///     章节对象访问名称
        /// </summary>
        public const string Section = "section";

        /// <summary>
        ///     设置对象访问名称
        /// </summary>
        public const string Settings = "settings";
    }

    /// <summary>
    ///     主题目录名称
    /// </summary>
    public static class Directories
    {
        public const string Templates = "templates";
        public const string Snippets = "snippets";
        public const string Locales = "locales";
        public const string Layout = "layout";
        public const string Assets = "assets";
        public const string Config = "config";
        public const string Sections = "sections";
    }

    /// <summary>
    ///     主题设置文件名
    /// </summary>
    public static class Config
    {
        public const string SettingsData = "settings_data.json";
        public const string SettingsSchema = "settings_schema.json";
        public const string SettingsPreset = "settings_preset.json";
    }
}