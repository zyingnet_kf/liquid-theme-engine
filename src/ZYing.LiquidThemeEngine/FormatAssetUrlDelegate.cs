﻿namespace ZYing.LiquidThemeEngine;

public delegate string FormatAssetUrlDelegate(string theme, string path, string version);