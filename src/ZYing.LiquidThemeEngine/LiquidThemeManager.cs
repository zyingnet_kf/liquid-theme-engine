﻿#region using

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text.Encodings.Web;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using Fluid;
using Fluid.Filters;
using Fluid.Parser;
using Fluid.Values;
using Newtonsoft.Json.Linq;
using ZYing.LiquidThemeEngine.Ast;
using ZYing.LiquidThemeEngine.Cache;
using ZYing.LiquidThemeEngine.Config.Schemas;
using ZYing.LiquidThemeEngine.Config.Values;
using ZYing.LiquidThemeEngine.Exceptions;
using ZYing.LiquidThemeEngine.Locales;
using ZYing.LiquidThemeEngine.Objects;
using ZYing.LiquidThemeEngine.Utils;

#endregion

namespace ZYing.LiquidThemeEngine;

public class LiquidThemeManager : ILiquidThemeManager
{
    private static readonly Regex TemplateNameRegex =
        new("^((?<dir>(\\w+))(/|\\\\))?(?<name>\\w+)\\.((?<suffix>(\\w+))\\.)?liquid$", RegexOptions.Compiled);

    public LiquidThemeManager(LiquidThemeEngineOptions options)
    {
        ThemeEngineOptions = options ?? throw new ArgumentNullException(nameof(options));
    }

    public LiquidThemeEngineOptions ThemeEngineOptions { get; }

    public void ClearCache(string theme)
    {
        if (_cache.TryGetValue(theme, out var cacheEntry)) cacheEntry.Clear();
    }

    public void ClearAllCache()
    {
        _cache.Clear();
    }

    public async Task RenderAsync(string template, TextWriter writer, TextEncoder encoder, TemplateContext context)
    {
        SetupContext(context);
        if (!ThemeEngineOptions.Parser.TryParse(template, out var tpl, out var error))
            tpl = ThemeEngineOptions.Parser.Parse(
                $"{{%raw%}}<pre style='color:red;'>template parse fail.{Environment.NewLine}{HttpUtility.HtmlEncode(error)}</pre>{{%endraw%}}");
        await tpl.RenderAsync(writer, encoder, context);
    }

    public async Task<ThemeSetting> GetThemeSettingAsync(string theme)
    {
        var cache = GetCacheEntry(theme);
        if (cache.ThemeSetting == null)
            using (await cache.LockAsync())
            {
                if (cache.ThemeSetting == null)
                {
                    var path = Helper.CombinePath(Constants.Directories.Config, Constants.Config.SettingsData);
                    var fileInfo = ThemeEngineOptions.FileProvider.GetFileInfo(theme, path);
                    if (!fileInfo.Exists) throw new ThemeFileNotFoundException(theme, path);
                    var json = await Helper.ReadAsStringAsync(fileInfo);
                    cache.ThemeSetting = ThemeSetting.Parse(json);
                }
            }

        return cache.ThemeSetting;
    }

    public async Task<IFluidTemplate> GetTemplateAsync(string theme, string path)
    {
        var cache = GetCacheEntry(theme);
        if (cache.TemplateCache.TryGetValue(path, out var template)) return template;

        var fileInfo = ThemeEngineOptions.FileProvider.GetFileInfo(theme, path);
        if (!fileInfo.Exists)
            return ThemeEngineOptions.Parser.Parse(
                $"{{%raw%}}<pre style='color:red;'>theme file not found! Path:{HttpUtility.HtmlEncode(path)}</pre>{{%endraw%}}");

        var content = await Helper.ReadAsStringAsync(fileInfo);

        if (!ThemeEngineOptions.Parser.TryParse(content, out template, out var error))
            return ThemeEngineOptions.Parser.Parse(
                $"{{%raw%}}<pre style='color:red;'>theme file parse fail.{Environment.NewLine}Path:{HttpUtility.HtmlEncode(path)}{Environment.NewLine}{HttpUtility.HtmlEncode(error)}</pre>{{%endraw%}}");
        cache.TemplateCache[path] = template;
        return template;
    }

    public async Task<ILocaleDictionary> GetLocaleDictionaryAsync(string theme, CultureInfo culture)
    {
        var name = culture.Name;
        var cache = GetCacheEntry(theme);
        if (cache.LocaleDictionarySet.TryGetValue(name, out var localeDictionary)) return localeDictionary;
        var dir = ThemeEngineOptions.FileProvider.GetDirectoryContents(theme, Constants.Directories.Locales);
        if (!dir.Exists) return LocaleDictionary.Empty;
        var dic = new LocaleDictionary();
        var search = new[] { "*.default.json", $"{culture.TwoLetterISOLanguageName}.json", $"{culture.Name}.json" };
        var loadFiles = new List<string>();

        foreach (var item in search)
        foreach (var file in dir)
        {
            if (file.IsDirectory) continue;
            if (!item.Like(item)) continue;
            var fileContent = await Helper.ReadAsStringAsync(file);
            var doc = JToken.Parse(fileContent);
            var filePath = Helper.CombinePath(Constants.Directories.Locales, file.Name);
            dic.MergeJsonIntoDictionary(doc);
            if (!loadFiles.Contains(filePath)) loadFiles.Add(filePath);
        }

        cache.LocaleDictionarySet[name] = dic;

        return dic;
    }

    public async Task<SectionSchema> GetSectionSchemaAsync(string theme, string section)
    {
        if (string.IsNullOrWhiteSpace(theme)) throw new ArgumentNullException(nameof(theme));
        if (string.IsNullOrWhiteSpace(section)) throw new ArgumentNullException(nameof(section));
        section = Helper.NormalTemplateFile(section);
        var fileName = Helper.CombinePath(Constants.Directories.Sections, section);
        var cache = GetCacheEntry(theme);
        if (cache.ThemeSectionSchemaCache.TryGetValue(fileName, out var sectionSchema)) return sectionSchema;
        var sectionTemplate = await GetTemplateAsync(theme, fileName);
        if (sectionTemplate is FluidTemplate template)
        {
            SchemaStatement statement = null;
            foreach (var item in template.Statements)
                if (item is SchemaStatement schemaStatement)
                {
                    statement = schemaStatement;
                    break;
                }

            if (statement != null && !string.IsNullOrWhiteSpace(statement.Text.ToString()))
                sectionSchema = SectionSchema.Parse(statement.Text.ToString());
        }

        if (sectionSchema == null)
            sectionSchema = new SectionSchema { Name = new FixedLocalizableText(Helper.NormalTemplateName(section)) };
        cache.ThemeSectionSchemaCache[fileName] = sectionSchema;
        return sectionSchema;
    }

    public async Task<TemplateSchema> GetTemplateSchemaAsync(string theme, string template)
    {
        if (string.IsNullOrWhiteSpace(theme)) throw new ArgumentNullException(nameof(theme));
        if (string.IsNullOrWhiteSpace(template)) throw new ArgumentNullException(nameof(template));
        template = Helper.NormalTemplateFile(template);
        var fileName = Helper.CombinePath(Constants.Directories.Templates, template);
        var cache = GetCacheEntry(theme);
        if (cache.TemplateSchemaCache.TryGetValue(fileName, out var sectionSchema)) return sectionSchema;
        var sectionTemplate = await GetTemplateAsync(theme, fileName);
        if (sectionTemplate is FluidTemplate fluidTemplate)
        {
            SchemaStatement statement = null;
            foreach (var item in fluidTemplate.Statements)
                if (item is SchemaStatement schemaStatement)
                {
                    statement = schemaStatement;
                    break;
                }

            if (statement != null && !string.IsNullOrWhiteSpace(statement.Text.ToString()))
                sectionSchema = TemplateSchema.Parse(statement.Text.ToString());
        }

        if (sectionSchema == null) sectionSchema = new TemplateSchema();
        cache.TemplateSchemaCache[fileName] = sectionSchema;
        return sectionSchema;
    }

    public string GetAssetUrl(string theme, string file)
    {
        var formatter = ThemeEngineOptions.AssetUrlFormatter;
        var path = Helper.CombinePath(Constants.Directories.Assets, file);
        var version = GetFileVersion(theme, path).ToString();
        var url = formatter(theme, file, version);
        return url;
    }

    public async Task RenderAsync(string theme, string template, TextWriter writer, TextEncoder encoder,
        TemplateContext context)
    {
        if (string.IsNullOrWhiteSpace(theme)) throw new ArgumentNullException(nameof(theme));
        if (string.IsNullOrEmpty(template)) throw new ArgumentNullException(nameof(template));
        if (writer == null) throw new ArgumentNullException(nameof(writer));
        if (encoder == null) throw new ArgumentNullException(nameof(encoder));
        if (context == null) throw new ArgumentNullException(nameof(context));
        SetupContext(context);
        template = Helper.NormalTemplateFile(template);

        context.SetLayout(Constants.DefaultLayout);
        context.SetTheme(theme);
        context.SetValue(Constants.ObjectNames.Template, CreateTemplateObject(template));

        var settings = await context.GetThemeSettingAsync();
        context.SetValue(Constants.ObjectNames.Settings, settings.Settings);
        template = Helper.CombinePath(Constants.Directories.Templates, template);
        var bodyTemplate = await GetTemplateAsync(theme, template);
        var body = await bodyTemplate.RenderAsync(context, encoder);
        if (context.TryGetLayout(out var layout))
        {
            context.SetBody(body);
            layout = Helper.NormalTemplateFile(layout);
            var layoutPath = Helper.CombinePath(Constants.Directories.Layout, layout);
            var layoutTemplate = await GetTemplateAsync(theme, layoutPath);
            await layoutTemplate.RenderAsync(writer, encoder, context);
        }
        else
        {
            await writer.WriteAsync(body);
        }
    }

    public async Task<string> DumpDataAsync(string theme, string template, TemplateContext context)
    {
        if (string.IsNullOrWhiteSpace(theme)) throw new ArgumentNullException(nameof(theme));
        if (string.IsNullOrEmpty(template)) throw new ArgumentNullException(nameof(template));
        SetupContext(context);
        template = Helper.NormalTemplateFile(template);

        context.SetLayout(Constants.DefaultLayout);
        context.SetTheme(theme);
        context.SetValue(Constants.ObjectNames.Template, CreateTemplateObject(template));

        var dic = new Dictionary<string, FluidValue>();

        if (context.Model != null)
        {
            var value = context.Model;
            if (value is ObjectValue objectValue)
            {
                var type = objectValue.Value.GetType();
                var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                var strategy = context.Options.MemberAccessStrategy;
                foreach (var property in properties)
                {
                    var name = strategy.MemberNameStrategy.Invoke(property);
                    var accessor = strategy.GetAccessor(type, name);
                    if (accessor != null)
                    {
                        if (accessor is IAsyncMemberAccessor asyncMemberAccessor)
                        {
                            var m = await asyncMemberAccessor.GetAsync(value, name, context);
                            dic[name] = FluidValue.Create(m, context.Options);
                        }
                        else
                        {
                            var m = accessor.Get(value, name, context);
                            dic[name] = FluidValue.Create(m, context.Options);
                        }
                    }
                }
            }
        }

        foreach (var name in context.ValueNames) dic[name] = context.GetValue(name);

        var input = new DictionaryValue(new DictionaryDictionaryFluidIndexable(dic, context.Options));
        var output = await MiscFilters.Json(input, FilterArguments.Empty, context);
        return output.ToStringValue();
    }

    public async Task RenderSectionAsync(RenderSectionCommand command)
    {
        if (command == null) throw new ArgumentNullException(nameof(command));
        if (string.IsNullOrWhiteSpace(command.Id)) throw new Exception("section 不能为空");
        if (command.ThemeSetting == null) command.ThemeSetting = await command.Context.GetThemeSettingAsync();

        var themeSetting = command.ThemeSetting;
        var sections = themeSetting.Sections;
        var writer = command.TextWriter;
        var id = command.Id;
        var theme = command.Theme;
        var context = command.Context;

        if (sections == null || !sections.TryGetValue(command.Id, out var setting))
        {
            await writer.WriteAsync($"<pre style='color:red;'>section  id '{command.Id}' not found in settings.</pre>");
            return;
        }

        if (!command.IsDynamic && command.Id != setting.Type)
        {
            await writer.WriteAsync(
                $"section type '{setting.Type}' and id '{id}' must be the same when a section is used inside a 'section' liquid tag."
            );
            return;
        }

        var path = Helper.CombinePath(Constants.Directories.Sections, Helper.NormalTemplateFile(setting.Type));
        var template = await GetTemplateAsync(theme, path);
        var schema = await GetSectionSchemaAsync(theme, setting.Type);

        var tag = "div";
        var tagId = $"liquid-section-{id}";
        var cls = "liquid-section";

        if (!string.IsNullOrWhiteSpace(schema.Tag)) tag = schema.Tag;
        if (!string.IsNullOrWhiteSpace(schema.Class)) cls += " " + schema.Class;

        var section = new Section { Id = id, Settings = setting.Settings };

        if (setting.Blocks != null && setting.BlockOrder != null)
        {
            section.Blocks = new List<Block>();
            foreach (var blockId in setting.BlockOrder)
                if (setting.Blocks.TryGetValue(blockId, out var block))
                    section.Blocks.Add(new Block
                    {
                        Id = blockId,
                        Type = block.Type,
                        Settings = block.Settings
                    });
        }

        await writer.WriteLineAsync();
        await writer.WriteAsync(
            $"<{tag} id=\"{HttpUtility.HtmlAttributeEncode(tagId)}\" class=\"{HttpUtility.HtmlAttributeEncode(cls)}\" >");
        context.IncrementSteps();

        try
        {
            context.EnterChildScope();
            context.SetValue(Constants.ObjectNames.Section, section);
            await template.RenderAsync(writer, command.TextEncoder, context);
        }
        finally
        {
            context.ReleaseScope();
        }

        await writer.WriteAsync($"</{tag}>");
    }

    public IReadOnlyCollection<string> GetAllTemplateSuffixes(string theme, string template)
    {
        var cache = GetCacheEntry(theme);
        template = Helper.NormalTemplateName(template);
        if (cache.TemplateSuffixsCache.TryGetValue(template, out var list)) return list;
        list = new List<string>();
        var files = ThemeEngineOptions.FileProvider.GetDirectoryContents(theme, Constants.Directories.Templates);
        var regex = new Regex($@"^{template}\.(?<suffix>[\w-]+)\.liquid$", RegexOptions.IgnoreCase);
        foreach (var file in files)
        {
            var name = file.Name;
            var m = regex.Match(name);
            if (!m.Success) continue;
            var suffix = m.Groups["suffix"].Value;
            list.Add(suffix);
        }

        cache.TemplateSuffixsCache[template] = list;
        return list;
    }

    private void SetupContext(TemplateContext context)
    {
        context.SetLiquidThemeManager(this);
    }

    public long GetFileVersion(string theme, string path)
    {
        var cache = GetCacheEntry(theme);
        if (cache.FileVersionCache.TryGetValue(path, out var fileVersionInfo)) return fileVersionInfo;

        fileVersionInfo = 0;
        var fileInfo = ThemeEngineOptions.FileProvider.GetFileInfo(theme, path);
        if (fileInfo.Exists) fileVersionInfo = fileInfo.LastModified.ToUnixTimeMilliseconds();
        cache.FileVersionCache[path] = fileVersionInfo;

        return fileVersionInfo;
    }

    private Template CreateTemplateObject(string path)
    {
        var m = TemplateNameRegex.Match(path);
        if (!m.Success) throw new Exception("模板名称格式不正确");
        var ret = new Template();
        var dir = m.Groups["dir"];
        var name = m.Groups["name"];
        var suffix = m.Groups["suffix"];
        if (dir.Success) ret.Directory = dir.Value;
        if (name.Success) ret.Name = name.Value;
        if (suffix.Success) ret.Suffix = suffix.Value;
        return ret;
    }

    #region Cache

    private readonly ConcurrentDictionary<string, CacheEntry> _cache = new();

    public CacheEntry GetCacheEntry(string theme)
    {
        return _cache.GetOrAdd(theme, theme =>
        {
            var cacheEntry = new CacheEntry();

            if (ThemeEngineOptions.TrackFileChanges)
            {
                Action<object> callback = null;
                var filter = $"**{Path.DirectorySeparatorChar}*.*";
                callback = c =>
                {
                    var entry = (CacheEntry)c;
                    entry.Callback?.Dispose();
                    var token = ThemeEngineOptions.FileProvider.Watch(theme, filter);
                    entry.Clear();
                    entry.Callback = token.RegisterChangeCallback(callback, c);
                };
                cacheEntry.Callback = ThemeEngineOptions.FileProvider.Watch(theme, filter)
                    .RegisterChangeCallback(callback, cacheEntry);
            }

            return cacheEntry;
        });
    }

    #endregion
}