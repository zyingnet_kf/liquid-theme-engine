﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Fluid;
using ZYing.LiquidThemeEngine.Config.Schemas;
using ZYing.LiquidThemeEngine.Config.Values;
using ZYing.LiquidThemeEngine.Locales;
using ZYing.LiquidThemeEngine.Utils;

namespace ZYing.LiquidThemeEngine.Cache;

public class CacheEntry
{
    public IDisposable Callback { get; set; }
    public ConcurrentDictionary<string, IFluidTemplate> TemplateCache { get; } = new();
    public ConcurrentDictionary<string, long> FileVersionCache { get; } = new();
    public ConcurrentDictionary<string, SectionSchema> ThemeSectionSchemaCache { get; } = new();
    public ConcurrentDictionary<string, TemplateSchema> TemplateSchemaCache { get; } = new();
    public LocaleDictionarySet LocaleDictionarySet { get; } = new();
    public ConcurrentDictionary<string, List<string>> TemplateSuffixsCache { get; } = new();
    public ThemeSetting ThemeSetting { get; set; }
    private SemaphoreSlim Semaphore { get; } = new(1);

    public void Clear()
    {
        TemplateCache.Clear();
        LocaleDictionarySet.Clear();
        FileVersionCache.Clear();
        ThemeSectionSchemaCache.Clear();
        TemplateSuffixsCache.Clear();
        TemplateSchemaCache.Clear();
        ThemeSetting = null;
    }

    public async Task<IDisposable> LockAsync()
    {
        await Semaphore.WaitAsync();
        return new DisposeAction(() => Semaphore.Release());
    }
}