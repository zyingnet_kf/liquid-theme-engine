﻿using Fluid;
using ZYing.LiquidThemeEngine.Filters;

namespace ZYing.LiquidThemeEngine;

public class LiquidTemplateOptions : TemplateOptions
{
    public LiquidTemplateOptions()
    {
        Filters.AddFilter("asset_url", UrlFilters.AssetUrl);
        Filters.AddFilter("t", AdditionalFilters.TranslationAsync);
        MemberAccessStrategy = AutoRegisterMemberAccessStrategy.Instance;
    }
}