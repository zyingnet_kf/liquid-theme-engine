﻿using System.Threading.Tasks;
using System.Web;
using Fluid;
using Fluid.Values;

namespace ZYing.LiquidThemeEngine.Filters;

public static class HtmlFilters
{
    public static ValueTask<FluidValue> ImgTagAsync(FluidValue input, FilterArguments arguments,
        TemplateContext context)
    {
        var value = input.ToStringValue();
        if (!string.IsNullOrWhiteSpace(value)) value = HttpUtility.HtmlAttributeEncode(value);
        return new StringValue($"<img src=\"{value}\" alt=\"\" />", false);
    }
}