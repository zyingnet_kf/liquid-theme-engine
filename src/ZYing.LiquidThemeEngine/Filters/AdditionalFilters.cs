﻿using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Fluid;
using Fluid.Values;

namespace ZYing.LiquidThemeEngine.Filters;

public static class AdditionalFilters
{
    private static readonly Regex InterpolationTemplateRegex =
        new("{{((\\s*)(?<id>[0-9a-zA-Z][0-9a-zA-Z-_]+)(\\s*))}}", RegexOptions.Compiled);

    private static string Render(string template, FilterArguments arguments)
    {
        return InterpolationTemplateRegex.Replace(template, m =>
        {
            var id = m.Groups["id"].Value;
            if (arguments.HasNamed(id))
            {
                var value = arguments[id];
                return value.ToStringValue();
            }

            return string.Empty;
        });
    }

    public static async ValueTask<FluidValue> TranslationAsync(FluidValue input, FilterArguments arguments,
        TemplateContext context)
    {
        var str = input.ToStringValue();
        var mgr = context.GetLiquidThemeManager();
        var theme = context.GetTheme();
        var dic = await mgr.GetLocaleDictionaryAsync(theme, context.CultureInfo);
        if (dic.TryGetValue(str, out var value))
        {
            var ret = Render(value, arguments);
            if (str.EndsWith("_html")) return new StringValue(ret, false);
            return new StringValue(ret);
        }

        return new StringValue($"translation missing: {str}");
    }
}