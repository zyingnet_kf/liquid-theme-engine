﻿using System.Threading.Tasks;
using Fluid;
using Fluid.Values;

namespace ZYing.LiquidThemeEngine.Filters;

public static class UrlFilters
{
    public static ValueTask<FluidValue> AssetUrl(FluidValue input, FilterArguments arguments, TemplateContext context)
    {
        if (context.TryGetLiquidThemeManager(out var mgr) && context.TryGetTheme(out var theme))
        {
            var url = mgr.GetAssetUrl(theme, input.ToStringValue());
            return new StringValue(url);
        }

        return input;
    }
}