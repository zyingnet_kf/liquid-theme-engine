﻿using System;
using System.IO;
using System.Text.Encodings.Web;
using Fluid;
using ZYing.LiquidThemeEngine.Config.Values;

namespace ZYing.LiquidThemeEngine;

public class RenderSectionCommand
{
    public RenderSectionCommand(string theme, TextWriter writer, TextEncoder encoder, TemplateContext context)
    {
        if (string.IsNullOrWhiteSpace(theme)) throw new ArgumentNullException(nameof(theme));
        Theme = theme;
        TextWriter = writer ?? throw new ArgumentNullException(nameof(writer));
        TextEncoder = encoder ?? throw new ArgumentNullException(nameof(encoder));
        Context = context ?? throw new ArgumentNullException(nameof(context));
    }

    public string Theme { get; }
    public string Id { get; set; }
    public ThemeSetting ThemeSetting { get; set; }
    public TextWriter TextWriter { get; }
    public TextEncoder TextEncoder { get; }
    public TemplateContext Context { get; }
    public bool IsDynamic { get; set; }
}