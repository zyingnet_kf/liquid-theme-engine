﻿using System;
using System.Threading.Tasks;
using Fluid;
using ZYing.LiquidThemeEngine.Config.Values;
using ZYing.LiquidThemeEngine.Reflection;

namespace ZYing.LiquidThemeEngine;

public static class TemplateContextExtensions
{
    #region Settings

    public static async Task<ThemeSetting> GetThemeSettingAsync(this TemplateContext context)
    {
        if (context.AmbientValues.TryGetValue(AmbientIndex.Setting, out var obj) && obj is ThemeSetting setting)
            return setting;
        var mgr = context.GetLiquidThemeManager();
        var theme = context.GetTheme();
        setting = await mgr.GetThemeSettingAsync(theme);
        context.AmbientValues[AmbientIndex.Setting] = setting;
        return setting;
    }

    #endregion

    #region AmbientIndex
    /// <summary>
    ///     环境数据索引
    /// </summary>
    private static class AmbientIndex
    {
        private const string Prefix = "$$";
        public const string Setting = Prefix + nameof(Setting);
        public const string LiquidThemeManager = Prefix + nameof(LiquidThemeManager);
        public const string Theme = Prefix + nameof(Theme);
        public const string Template = Prefix + nameof(Template);
        public const string Layout = Prefix + nameof(Layout);
        public const string Body = Prefix + nameof(Body);
    }
    #endregion
    
    #region LiquidThemeManager

    public static T SetLiquidThemeManager<T>(this T context, ILiquidThemeManager manager) where T : TemplateContext
    {
        context.AmbientValues[AmbientIndex.LiquidThemeManager] = manager;
        return context;
    }

    public static bool TryGetLiquidThemeManager(this TemplateContext context, out ILiquidThemeManager manager)
    {
        if (context == null) throw new ArgumentNullException(nameof(context));
        if (context.AmbientValues.TryGetValue(AmbientIndex.LiquidThemeManager, out var obj) &&
            obj is ILiquidThemeManager mgr)
        {
            manager = mgr;
            return true;
        }

        manager = default;
        return false;
    }

    public static ILiquidThemeManager GetLiquidThemeManager(this TemplateContext context)
    {
        if (TryGetLiquidThemeManager(context, out var mgr)) return mgr;
        throw new Exception("没有在上下文中找到 LiquidThemeManager");
    }

    #endregion

    #region Theme

    public static T SetTheme<T>(this T context, string theme) where T : TemplateContext
    {
        if (context == null) throw new ArgumentNullException(nameof(context));
        if (string.IsNullOrWhiteSpace(theme)) throw new ArgumentNullException(nameof(theme));
        context.AmbientValues[AmbientIndex.Theme] = theme;
        return context;
    }

    public static bool TryGetTheme(this TemplateContext context, out string theme)
    {
        if (context == null) throw new ArgumentNullException(nameof(context));
        if (context.AmbientValues.TryGetValue(AmbientIndex.Theme, out var obj) && obj is string str &&
            !string.IsNullOrWhiteSpace(str))
        {
            theme = str;
            return true;
        }

        theme = default;
        return false;
    }

    public static string GetTheme(this TemplateContext context)
    {
        if (context.TryGetTheme(out var theme)) return theme;
        throw new Exception("没有在上下文中找到 theme");
    }

    #endregion

    #region Layout

    public static T SetLayout<T>(this T context, string layout) where T : TemplateContext
    {
        if (context == null) throw new ArgumentNullException(nameof(context));
        if (string.IsNullOrWhiteSpace(layout))
            context.AmbientValues.Remove(AmbientIndex.Layout);
        else
            context.AmbientValues[AmbientIndex.Layout] = layout.Trim();
        return context;
    }

    public static bool TryGetLayout(this TemplateContext context, out string layout)
    {
        if (context == null) throw new ArgumentNullException(nameof(context));
        if (context.AmbientValues.TryGetValue(AmbientIndex.Layout, out var obj) && obj is string str)
        {
            layout = str;
            return true;
        }

        layout = default;
        return false;
    }

    #endregion

    #region Body

    public static T SetBody<T>(this T context, string body) where T : TemplateContext
    {
        if (context == null) throw new ArgumentNullException(nameof(context));
        if (string.IsNullOrWhiteSpace(body))
            context.AmbientValues.Remove(AmbientIndex.Body);
        else
            context.AmbientValues[AmbientIndex.Body] = body.Trim();
        return context;
    }

    public static string GetBody(this TemplateContext context)
    {
        if (context.AmbientValues.TryGetValue(AmbientIndex.Body, out var obj) && obj is string str) return str;
        return string.Empty;
    }

    #endregion

    #region Scope
    public static Scope GetLocalScope(this TemplateContext context)
    {
        if (context == null) return null;
        dynamic ctx = ReflectionDynamicObject.Wrap(context);
        return ctx.LocalScope;
    }
    public static void SetLocalScope(this TemplateContext context, Scope scope)
    {
        if (context == null) throw new ArgumentNullException(nameof(context));
        if (scope == null) throw new ArgumentNullException(nameof(scope));
        dynamic ctx = ReflectionDynamicObject.Wrap(context);
        ctx.LocalScope = scope;
    }

    public static Scope GetRootScope(this TemplateContext context)
    {
        if (context == null) return null;
        dynamic ctx = ReflectionDynamicObject.Wrap(context);
        return ctx.RootScope;
    }
    #endregion
}