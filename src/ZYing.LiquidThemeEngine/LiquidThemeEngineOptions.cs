﻿using System;
using ZYing.LiquidThemeEngine.FileProviders;

namespace ZYing.LiquidThemeEngine;

public class LiquidThemeEngineOptions
{
    private FormatAssetUrlDelegate _assetUrlFormatter = FormatAssetUrl;
    private ILiquidThemeFileProvider _fileProvider = NullLiquidThemeFileProvider.Instance;

    public LiquidThemeEngineOptions()
    {
        TrackFileChanges = true;
    }

    public ILiquidThemeFileProvider FileProvider
    {
        get => _fileProvider;
        set => _fileProvider = value ?? throw new ArgumentNullException(nameof(value));
    }

    public FormatAssetUrlDelegate AssetUrlFormatter
    {
        get => _assetUrlFormatter;
        set => _assetUrlFormatter = value ?? throw new ArgumentNullException(nameof(value));
    }

    public bool TrackFileChanges { get; set; }
    public LiquidTemplateOptions TemplateOptions { get; } = new();
    public LiquidTemplateParser Parser { get; set; } = new();

    private static string FormatAssetUrl(string theme, string url, string version)
    {
        if (string.IsNullOrWhiteSpace(version)) return $"/themes/{theme}/assets/{url}";
        return $"/themes/{theme}/assets/{url}?v={version}";
    }
}