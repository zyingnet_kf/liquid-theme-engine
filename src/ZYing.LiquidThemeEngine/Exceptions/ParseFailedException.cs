﻿namespace ZYing.LiquidThemeEngine.Exceptions;

public class ParseFailedException : ThemeException
{
    public ParseFailedException(string theme, string file, string error) : base(GetMessage(theme, file, error))
    {
        Theme = theme;
        File = file;
        Error = error;
    }

    public ParseFailedException(string theme, string file, string raw, string error) : base(GetMessage(theme, file, raw,
        error))
    {
        Theme = theme;
        File = file;
        Raw = raw;
        Error = error;
    }

    public string Theme { get; }
    public string File { get; }
    public string Error { get; }
    public string Raw { get; }

    private static string GetMessage(string theme, string file, string error)
    {
        return $"Liquid解析失败。主题：{theme} 文件：{file} 错误：{error}";
    }

    private static string GetMessage(string theme, string file, string raw, string error)
    {
        return $"Liquid解析失败。主题：{theme} 文件：{file} 内容：{raw} 错误：{error}";
    }
}