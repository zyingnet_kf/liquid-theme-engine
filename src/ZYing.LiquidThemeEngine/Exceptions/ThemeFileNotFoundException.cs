﻿using System;

namespace ZYing.LiquidThemeEngine.Exceptions;

public class ThemeFileNotFoundException : ThemeException
{
    public ThemeFileNotFoundException(string theme, string file) : base($"主题文件未找到。主题:{theme} 文件:{file}")
    {
        Theme = !string.IsNullOrWhiteSpace(theme) ? theme : throw new ArgumentNullException(nameof(theme));
        File = !string.IsNullOrWhiteSpace(file) ? file : throw new ArgumentNullException(nameof(file));
    }

    public string Theme { get; }
    public string File { get; }
}