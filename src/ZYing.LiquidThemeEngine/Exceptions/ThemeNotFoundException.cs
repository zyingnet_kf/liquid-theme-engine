﻿namespace ZYing.LiquidThemeEngine.Exceptions;

public class ThemeNotFoundException : ThemeException
{
    public ThemeNotFoundException(string theme) : base($"主题：{theme} 未找到")
    {
        Theme = theme;
    }

    public string Theme { get; }
}