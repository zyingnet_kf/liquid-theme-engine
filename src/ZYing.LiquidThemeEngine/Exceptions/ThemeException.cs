﻿using System;

namespace ZYing.LiquidThemeEngine.Exceptions;

public class ThemeException : Exception
{
    public ThemeException()
    {
    }

    public ThemeException(string message) : base(message)
    {
    }

    public ThemeException(string message, Exception innerException) : base(message, innerException)
    {
    }
}