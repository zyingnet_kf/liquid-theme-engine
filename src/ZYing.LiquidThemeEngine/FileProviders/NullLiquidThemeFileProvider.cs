﻿using System;
using System.IO;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Primitives;

namespace ZYing.LiquidThemeEngine.FileProviders;

public class NullLiquidThemeFileProvider : ILiquidThemeFileProvider
{
    private NullLiquidThemeFileProvider()
    {
    }

    public static NullLiquidThemeFileProvider Instance { get; } = new();

    public IDirectoryContents GetDirectoryContents(string theme, string path)
    {
        return NotFoundDirectoryContents.Singleton;
    }

    public IFileInfo GetFileInfo(string theme, string path)
    {
        if (string.IsNullOrWhiteSpace(path)) throw new ArgumentNullException(nameof(path));
        var fn = Path.Combine(theme, path);
        return new NotFoundFileInfo(fn);
    }

    public IChangeToken Watch(string theme, string filter)
    {
        return NullChangeToken.Singleton;
    }
}