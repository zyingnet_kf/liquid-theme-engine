﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Primitives;
using ZYing.LiquidThemeEngine.Utils;

namespace ZYing.LiquidThemeEngine.FileProviders;

public class LiquidThemeFileProvider : ILiquidThemeFileProvider
{
    public LiquidThemeFileProvider(IFileProvider fileProvider, string prefix)
    {
        FileProvider = fileProvider ?? throw new ArgumentNullException(nameof(fileProvider));
        Prefix = prefix ?? throw new ArgumentOutOfRangeException(nameof(prefix));
    }

    public LiquidThemeFileProvider(IFileProvider fileProvider)
    {
        FileProvider = fileProvider ?? throw new ArgumentNullException(nameof(fileProvider));
    }

    public IFileProvider FileProvider { get; }
    public string Prefix { get; } = string.Empty;

    public IFileInfo GetFileInfo(string theme, string path)
    {
        if (string.IsNullOrWhiteSpace(theme)) throw new ArgumentNullException(nameof(theme));
        if (string.IsNullOrWhiteSpace(path)) throw new ArgumentNullException(nameof(path));
        var list = new List<string>();
        if (!string.IsNullOrWhiteSpace(Prefix)) list.Add(Prefix);
        list.Add(theme);
        list.Add(path);
        var full = Path.Combine(list.ToArray());
        var file = FileProvider.GetFileInfo(full);
        return new FileInfo(file);
    }

    public IDirectoryContents GetDirectoryContents(string theme, string path)
    {
        if (string.IsNullOrWhiteSpace(theme)) throw new ArgumentNullException(nameof(theme));
        if (string.IsNullOrWhiteSpace(path)) throw new ArgumentNullException(nameof(path));
        var list = new List<string>();
        if (!string.IsNullOrWhiteSpace(Prefix)) list.Add(Prefix);
        list.Add(theme);
        list.Add(path);
        var full = Path.Combine(list.ToArray());
        var dir = FileProvider.GetDirectoryContents(full);
        return new DirectoryContents(dir);
    }

    public IChangeToken Watch(string theme, string filter)
    {
        if (string.IsNullOrWhiteSpace(theme)) throw new ArgumentNullException(nameof(theme));
        if (string.IsNullOrWhiteSpace(filter)) throw new ArgumentNullException(nameof(filter));
        var list = new List<string>();
        if (!string.IsNullOrWhiteSpace(Prefix)) list.Add(Prefix);
        list.Add(theme);
        list.Add(filter);
        var full = Helper.CombinePath(list.ToArray());
        return FileProvider.Watch(full);
    }

    #region LiquidThemeFileInfo

    private readonly struct FileInfo : IFileInfo
    {
        private readonly IFileInfo _fileInfo;

        public FileInfo(IFileInfo fileInfo)
        {
            _fileInfo = fileInfo ?? throw new ArgumentNullException(nameof(fileInfo));
        }

        public bool Exists => _fileInfo.Exists;

        public long Length => _fileInfo.Length;

        public string Name => _fileInfo.Name;

        public DateTimeOffset LastModified => _fileInfo.LastModified;

        public bool IsDirectory => _fileInfo.IsDirectory;

        public string PhysicalPath => _fileInfo.PhysicalPath;

        public Stream CreateReadStream()
        {
            return _fileInfo.CreateReadStream();
        }
    }

    #endregion

    #region LiquidThemeDirectoryContents

    private readonly struct DirectoryContents : IDirectoryContents
    {
        private readonly IDirectoryContents _directoryContents;

        public DirectoryContents(IDirectoryContents directoryContents)
        {
            _directoryContents = directoryContents ?? throw new ArgumentNullException(nameof(directoryContents));
        }

        public bool Exists => _directoryContents.Exists;

        public IEnumerator<IFileInfo> GetEnumerator()
        {
            if (_directoryContents.Exists)
                foreach (var item in _directoryContents)
                    yield return new FileInfo(item);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    #endregion
}