﻿using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Primitives;

namespace ZYing.LiquidThemeEngine.FileProviders;

public interface ILiquidThemeFileProvider
{
    IFileInfo GetFileInfo(string theme, string path);
    IDirectoryContents GetDirectoryContents(string theme, string path);
    IChangeToken Watch(string theme, string filter);
}