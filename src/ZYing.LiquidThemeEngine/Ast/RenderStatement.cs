﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Fluid;
using Fluid.Ast;
using Fluid.Values;
using ZYing.LiquidThemeEngine.Utils;

namespace ZYing.LiquidThemeEngine.Ast;

public class RenderStatement : Statement
{
    public const string ViewExtension = ".liquid";

    public RenderStatement(string path, Expression with = null, Expression @for = null, string alias = null,
        IList<AssignStatement> assignStatements = null)
    {
        Path = path;
        With = with;
        For = @for;
        Alias = alias;
        AssignStatements = assignStatements;
    }

    public string Path { get; }
    public IList<AssignStatement> AssignStatements { get; }
    public Expression With { get; }
    public Expression For { get; }
    public string Alias { get; }


    private async Task<IFluidTemplate> GetTemplateAsync(TemplateContext context, string name)
    {
        var mgr = context.GetLiquidThemeManager();
        var theme = context.GetTheme();
        name = Helper.NormalTemplateFile(name);
        var path = Helper.CombinePath(Constants.Directories.Snippets, name);
        return await mgr.GetTemplateAsync(theme, path);
    }

    public override async ValueTask<Completion> WriteToAsync(TextWriter writer, TextEncoder encoder,
        TemplateContext context)
    {
        context.IncrementSteps();

        var relativePath = Path;

        if (!relativePath.EndsWith(ViewExtension, StringComparison.OrdinalIgnoreCase)) relativePath += ViewExtension;

        var template = await GetTemplateAsync(context, relativePath);
        var name = System.IO.Path.GetFileNameWithoutExtension(relativePath);
        context.EnterChildScope();
        var previousScope = context.GetLocalScope();

        try
        {
            if (With != null)
            {
                var with = await With.EvaluateAsync(context);

                var localScope = new Scope(context.GetRootScope());
                previousScope.CopyTo(localScope);
                context.SetLocalScope(localScope);
                context.SetValue(Alias ?? name, with);
                await template.RenderAsync(writer, encoder, context);
            }
            else if (AssignStatements != null)
            {
                var length = AssignStatements.Count;
                for (var i = 0; i < length; i++)
                {
                    await AssignStatements[i].WriteToAsync(writer, encoder, context);
                }

                var localScope = new Scope(context.GetRootScope());
                previousScope.CopyTo(localScope);
                context.SetLocalScope(localScope);

                await template.RenderAsync(writer, encoder, context);
            }
            else if (For != null)
            {
                try
                {
                    var forloop = new ForLoopValue();

                    var list = (await For.EvaluateAsync(context)).Enumerate(context).ToList();

                    var localScope = new Scope(context.GetRootScope());
                    previousScope.CopyTo(localScope);
                    context.SetLocalScope(localScope);

                    var length = forloop.Length = list.Count;

                    context.SetValue("forloop", forloop);

                    for (var i = 0; i < length; i++)
                    {
                        context.IncrementSteps();

                        var item = list[i];

                        context.SetValue(Alias ?? name, item);

                        // Set helper variables
                        forloop.Index = i + 1;
                        forloop.Index0 = i;
                        forloop.RIndex = length - i - 1;
                        forloop.RIndex0 = length - i;
                        forloop.First = i == 0;
                        forloop.Last = i == length - 1;

                        //await _cachedTemplate.Template.RenderAsync(writer, encoder, context);
                        await template.RenderAsync(writer, encoder, context);

                        // Restore the forloop property after every statement in case it replaced it,
                        // for instance if it contains a nested for loop
                        context.SetValue("forloop", forloop);
                    }
                }
                finally
                {
                    context.GetLocalScope().Delete("forloop");
                }
            }
            else
            {
                var localScope = new Scope(context.GetRootScope());
                previousScope.CopyTo(localScope);
                context.SetLocalScope(localScope);

                await template.RenderAsync(writer, encoder, context);
            }
        }
        finally
        {
            context.SetLocalScope(previousScope);
            context.ReleaseScope();
        }

        return Completion.Normal;
    }
}