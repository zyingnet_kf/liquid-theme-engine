﻿using System.IO;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Fluid;
using Fluid.Ast;
using Parlot;

namespace ZYing.LiquidThemeEngine.Ast;

public class SchemaStatement : Statement
{
    private readonly TextSpan _text;

    public SchemaStatement(in TextSpan text)
    {
        _text = text;
    }

    public ref readonly TextSpan Text => ref _text;

    public override ValueTask<Completion> WriteToAsync(TextWriter writer, TextEncoder encoder, TemplateContext context)
    {
        return Normal();
    }
}