﻿using System;
using System.Collections.Generic;
using Fluid;

namespace ZYing.LiquidThemeEngine;

public class AutoRegisterMemberAccessStrategy : DefaultMemberAccessStrategy
{
    public static readonly AutoRegisterMemberAccessStrategy Instance = new();
    private readonly object _synLock = new();
    private Dictionary<MemberNameStrategy, HashSet<Type>> _registered = new();

    public override IMemberAccessor GetAccessor(Type type, string name)
    {
        var accessor = base.GetAccessor(type, name);
        if (accessor != null) return accessor;
        if (_registered.TryGetValue(MemberNameStrategy, out var set) && set.Contains(type)) return null;

        lock (_synLock)
        {
            this.Register(type);
            var temp = new Dictionary<MemberNameStrategy, HashSet<Type>>(_registered);
            set = temp.TryGetValue(MemberNameStrategy, out set) ? new HashSet<Type>(set) : new HashSet<Type>();
            set.Add(type);
            temp[MemberNameStrategy] = set;
            _registered = temp;
        }

        return base.GetAccessor(type, name);
    }
}