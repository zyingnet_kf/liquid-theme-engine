﻿using System.IO;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Fluid;
using Fluid.Ast;
using Fluid.Parser;
using Parlot.Fluent;
using ZYing.LiquidThemeEngine.Ast;
using static Parlot.Fluent.Parsers;
using RenderStatement = ZYing.LiquidThemeEngine.Ast.RenderStatement;

namespace ZYing.LiquidThemeEngine;

/// <summary>
///     Liquid 语法解析器
/// </summary>
public class LiquidTemplateParser : FluidParser
{
    public LiquidTemplateParser()
    {
        RegisteredTags["schema"] = SchemaTag();

        RegisterExpressionTag("layout", Layout);
        RegisterEmptyTag("render_body", RenderBody);
        RegisterEmptyTag("render_index", RenderIndex);
        RegisterExpressionTag("section", Section);


        var StringAfterRender = String.ElseError(ErrorMessages.ExpectedStringRender);
        var RenderTag = OneOf(
                    StringAfterRender
                        .AndSkip(Comma)
                        .And(Separated(Comma,
                            Identifier.AndSkip(Colon).And(Primary)
                                .Then(static x => new AssignStatement(x.Item1, x.Item2))))
                        .Then(x => new RenderStatement(x.Item1.ToString(), null, null, null, x.Item2)),
                    StringAfterRender
                        .AndSkip(Terms.Text("with")).And(Primary)
                        .And(ZeroOrOne(Terms.Text("as").SkipAnd(Identifier))).Then(x =>
                            new RenderStatement(x.Item1.ToString(), with: x.Item2, alias: x.Item3)),
                    StringAfterRender.AndSkip(Terms.Text("for")).And(Primary)
                        .And(ZeroOrOne(Terms.Text("as").SkipAnd(Identifier))).Then(x =>
                            new RenderStatement(x.Item1.ToString(), @for: x.Item2, alias: x.Item3)),
                    StringAfterRender.Then(x => new RenderStatement(x.ToString()))
                ).AndSkip(TagEnd)
                .Then<Statement>(x => x)
                .ElseError("Invalid 'render' tag")
            ;
        RegisteredTags["render"] = RenderTag;
    }


    private static async ValueTask<Completion> Section(Expression expression, TextWriter writer, TextEncoder encoder,
        TemplateContext context)
    {
        var section = (await expression.EvaluateAsync(context)).ToStringValue();
        var mgr = context.GetLiquidThemeManager();
        var theme = context.GetTheme();
        var cmd = new RenderSectionCommand(theme, writer, encoder, context) { Id = section };
        await mgr.RenderSectionAsync(cmd);
        return Completion.Normal;
    }

    private static Parser<Statement> SchemaTag()
    {
        return TagEnd
            .SkipAnd(AnyCharBefore(CreateTag("endschema")))
            .AndSkip(CreateTag("endschema").ElseError("'{% endschema %}' was expected"))
            .Then<Statement>(x => new SchemaStatement(x))
            .ElseError("Invalid 'schema' tag");
    }

    private static async ValueTask<Completion> RenderIndex(TextWriter writer, TextEncoder encoder,
        TemplateContext context)
    {
        var theme = context.GetTheme();
        var mgr = context.GetLiquidThemeManager();
        var settings = await context.GetThemeSettingAsync();
        if (settings.Sections != null && settings.ContentForIndex != null)
        {
            var cmd = new RenderSectionCommand(theme, writer, encoder, context)
            {
                ThemeSetting = settings,
                IsDynamic = true
            };
            foreach (var id in settings.ContentForIndex)
            {
                cmd.Id = id;
                await mgr.RenderSectionAsync(cmd);
            }
        }

        return Completion.Normal;
    }

    private static async ValueTask<Completion> RenderBody(TextWriter writer, TextEncoder encoder,
        TemplateContext context)
    {
        var body = context.GetBody();
        await writer.WriteAsync(body);
        return Completion.Normal;
    }

    private static async ValueTask<Completion> Layout(Expression expression, TextWriter writer, TextEncoder encoder,
        TemplateContext context)
    {
        var layout = await expression.EvaluateAsync(context);
        context.SetLayout(layout.ToStringValue());
        return Completion.Normal;
    }
}