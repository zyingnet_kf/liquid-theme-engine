﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Extensions.FileProviders;

namespace ZYing.LiquidThemeEngine.Utils;

internal static class Helper
{
    public static string CombinePath(params string[] paths)
    {
        return Path.Combine(paths);
    }

    public static bool Like(this string str, string pattern)
    {
        return new Regex(
            "^" + Regex.Escape(pattern).Replace(@"\*", ".*").Replace(@"\?", ".") + "$",
            RegexOptions.IgnoreCase | RegexOptions.Singleline
        ).IsMatch(str);
    }

    public static async Task<string> ReadAsStringAsync(IFileInfo fileInfo)
    {
        using var stream = fileInfo.CreateReadStream();
        using var streamReader = new StreamReader(stream, Encoding.UTF8, true);
        return await streamReader.ReadToEndAsync();
    }

    public static string NormalTemplateName(string name)
    {
        if (string.IsNullOrWhiteSpace(name)) return string.Empty;

        if (name.EndsWith(Constants.TemplateExtension, StringComparison.OrdinalIgnoreCase))
        {
            var len = name.Length - Constants.TemplateExtension.Length;
            return name.Substring(0, len);
        }

        return name;
    }

    public static string NormalTemplateFile(string name)
    {
        if (string.IsNullOrWhiteSpace(name)) return string.Empty;

        if (name.EndsWith(Constants.TemplateExtension, StringComparison.OrdinalIgnoreCase)) return name;
        return name + Constants.TemplateExtension;
    }
}