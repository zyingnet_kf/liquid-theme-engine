﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Fluid;
using ZYing.LiquidThemeEngine.Config.Schemas;
using ZYing.LiquidThemeEngine.Config.Values;
using ZYing.LiquidThemeEngine.Locales;

namespace ZYing.LiquidThemeEngine;

public interface ILiquidThemeManager
{
    LiquidThemeEngineOptions ThemeEngineOptions { get; }
    void ClearCache(string theme);
    void ClearAllCache();
    string GetAssetUrl(string theme, string file);
    Task<IFluidTemplate> GetTemplateAsync(string theme, string path);
    Task<ThemeSetting> GetThemeSettingAsync(string theme);
    IReadOnlyCollection<string> GetAllTemplateSuffixes(string theme, string template);
    Task<ILocaleDictionary> GetLocaleDictionaryAsync(string theme, CultureInfo culture);
    Task<SectionSchema> GetSectionSchemaAsync(string theme, string section);
    Task<TemplateSchema> GetTemplateSchemaAsync(string theme, string template);
    Task RenderAsync(string template, TextWriter writer, TextEncoder encoder, TemplateContext context);
    Task RenderAsync(string theme, string template, TextWriter writer, TextEncoder encoder, TemplateContext context);
    Task RenderSectionAsync(RenderSectionCommand command);
    Task<string> DumpDataAsync(string theme, string template, TemplateContext context);
}