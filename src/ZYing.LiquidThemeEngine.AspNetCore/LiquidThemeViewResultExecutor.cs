﻿using System;
using System.IO;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Fluid;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;

namespace ZYing.LiquidThemeEngine.AspNetCore;

public class LiquidThemeViewResultExecutor : IActionResultExecutor<LiquidThemeViewResult>
{
    private readonly HtmlEncoder _htmlEncoder;
    private readonly ILiquidThemeManager _manager;

    public LiquidThemeViewResultExecutor(ILiquidThemeManager manager, HtmlEncoder htmlEncoder)
    {
        _manager = manager;
        _htmlEncoder = htmlEncoder;
    }

    public async Task ExecuteAsync(ActionContext context, LiquidThemeViewResult result)
    {
        if (context == null) throw new ArgumentNullException(nameof(context));

        if (result == null) throw new ArgumentNullException(nameof(result));

        result.StatusCode ??= 200;
        var response = context.HttpContext.Response;
        response.StatusCode = result.StatusCode.Value;
        var templateContext = new TemplateContext(_manager.ThemeEngineOptions.TemplateOptions);
        if (result.ViewData != null)
            foreach (var item in result.ViewData)
                templateContext.SetValue(item.Key, item.Value);
        switch (result.OutputMode)
        {
            case OutputMode.Render:
                using (var writer = new StreamWriter(response.BodyWriter.AsStream(), Encoding.UTF8, leaveOpen: true))
                {
                    await _manager.RenderAsync(result.Theme, result.Template, writer, _htmlEncoder, templateContext);
                    await writer.FlushAsync();
                }

                break;
            case OutputMode.DumpData:
            {
                response.ContentType = "application/json";
                var json = await _manager.DumpDataAsync(result.Theme, result.Template, templateContext);
                await response.WriteAsync(json);
            }
                break;
        }
    }
}