﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.DependencyInjection;

namespace ZYing.LiquidThemeEngine.AspNetCore;

public class LiquidThemeViewResult : IActionResult, IStatusCodeActionResult
{
    public LiquidThemeViewResult(string theme, string view)
    {
        Theme = theme ?? throw new ArgumentNullException(nameof(theme));
        Template = !string.IsNullOrWhiteSpace(view) ? view : throw new ArgumentNullException(nameof(view));
        OutputMode = OutputMode.Render;
    }

    public string Theme { get; }
    public string Template { get; }
    public ViewDataDictionary ViewData { get; set; }
    public OutputMode OutputMode { get; set; }

    public async Task ExecuteResultAsync(ActionContext context)
    {
        if (context == null) throw new ArgumentNullException(nameof(context));

        var executor = context.HttpContext.RequestServices
            .GetRequiredService<IActionResultExecutor<LiquidThemeViewResult>>();
        await executor.ExecuteAsync(context, this);
    }

    public int? StatusCode { get; set; }
}