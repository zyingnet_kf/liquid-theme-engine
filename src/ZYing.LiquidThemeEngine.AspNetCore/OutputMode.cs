﻿namespace ZYing.LiquidThemeEngine.AspNetCore;

public enum OutputMode
{
    Render,
    DumpData
}