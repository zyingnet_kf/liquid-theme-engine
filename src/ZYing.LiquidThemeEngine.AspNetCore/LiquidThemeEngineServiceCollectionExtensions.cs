﻿using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.DependencyInjection;

namespace ZYing.LiquidThemeEngine.AspNetCore;

public static class LiquidThemeEngineServiceCollectionExtensions
{
    public static IServiceCollection AddLiquidThemeEngine(this IServiceCollection services)
    {
        services.AddTransient<IActionResultExecutor<LiquidThemeViewResult>, LiquidThemeViewResultExecutor>();
        return services;
    }
}