﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using ZYing.LiquidThemeEngine.Locales;

namespace ZYing.LiquidThemeEngine.Test.Locales
{
    [TestClass]
    public class DictionaryLocalizableTextTest
    {
        [TestMethod]
        public void TestSerialize()
        {
            var dic = new DictionaryLocalizableText
            {
                { "zh-cn", "中文" },
                { "en-us", "English" }
            };
            var json = JsonConvert.SerializeObject(dic);
            Assert.AreEqual("{\"en-us\":\"English\",\"zh-cn\":\"中文\"}", json);
        }

        [TestMethod]
        public void TestDeserialize()
        {
            var lang = "{\"en-us\":\"English\",\"zh-cn\":\"中文\"}";
            var locale = JsonConvert.DeserializeObject<ILocalizableText>(lang);
            Assert.IsNotNull(locale);
            Assert.AreEqual("中文", locale.Locale("zh-cn"));
            Assert.AreEqual("English", locale.Locale("en-us"));
        }
    }
}