﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using ZYing.LiquidThemeEngine.Locales;

namespace ZYing.LiquidThemeEngine.Test.Locales
{
    [TestClass]
    public class FixedLocalizableTextTest
    {
        [TestMethod]
        public void TestSerialize()
        {
            var txt = new FixedLocalizableText("Test");
            var json = JsonConvert.SerializeObject(txt);
            Assert.AreEqual("\"Test\"", json);
        }

        [TestMethod]
        public void TestDeserialize()
        {
            var lang = JsonConvert.SerializeObject("Test");
            var locale = JsonConvert.DeserializeObject<ILocalizableText>(lang);
            Assert.IsNotNull(locale);
            Assert.AreEqual("Test", locale.Locale("zh-cn"));
        }
    }
}